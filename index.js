/**
 * Bài 1: Tính điểm
 */
document.getElementById("btn-ketqua").addEventListener("click", function () {
  var diemchuanVal = document.getElementById("txt-diemchuan").value * 1;
  var khuvucVal = document.getElementById("txt-khuvuc").value * 1;
  var doituongVal = document.getElementById("txt-doituong").value * 1;
  var diem1Val = document.getElementById("txt-diem1").value * 1;
  var diem2Val = document.getElementById("txt-diem2").value * 1;
  var diem3Val = document.getElementById("txt-diem3").value * 1;
  console.log({
    diemchuanVal,
    khuvucVal,
    doituongVal,
    diem1Val,
    diem2Val,
    diem3Val,
  });

  if (Check_Zero(diem1Val) && Check_Zero(diem2Val) && Check_Zero(diem3Val)) {
    var DiemTong = diem1Val + diem2Val + diem3Val + khuvucVal + doituongVal;
    if (DiemTong >= diemchuanVal) {
      document.getElementById("result_01").innerHTML =
        "Bạn đã đậu, tổng điểm là: " + DiemTong;
    } else {
      document.getElementById("result_01").innerHTML =
        "Bạn đã rớt, tổng điểm là: " + DiemTong;
    }
  } else {
    document.getElementById("result_01").innerHTML =
      "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
  }
});
function Check_Zero(diem) {
  return !(diem <= 0);
}
/**
 * Bài 2: Tính tiền điện
 */
document.getElementById("btn-tiendien").addEventListener("click", function () {
  const kw_1 = 500,
    kw_2 = 650,
    kw_3 = 850,
    kw_4 = 1100,
    kw_5 = 1300;
  var HoTen = document.getElementById("txt-HoTen").value;
  var DienNang = document.getElementById("txt-DienNang").value * 1;
  var TienDien = 0;
  if (0 < DienNang && DienNang <= 50) {
    TienDien = DienNang * kw_1;
  } else if (DienNang > 50 && DienNang <= 100) {
    TienDien = 50 * kw_1 + (DienNang - 50) * kw_2;
  } else if (DienNang > 100 && DienNang <= 200) {
    TienDien = 50 * kw_1 + 50 * kw_2 + (DienNang - 100) * kw_3;
  } else if (DienNang > 200 && DienNang <= 350) {
    TienDien = 50 * kw_1 + 50 * kw_2 + 100 * kw_3 + (DienNang - 200) * kw_4;
  } else if (DienNang > 350) {
    TienDien =
      50 * kw_1 + 50 * kw_2 + 100 * kw_3 + 150 * kw_4 + (DienNang - 350) * kw_5;
  } else {
    alert("Số kw không hợp lệ! Vui lòng nhập lại");
  }
  TienDien = new Intl.NumberFormat("vn-VN").format(TienDien);
  document.getElementById("result_02").innerHTML =
    "Họ tên: " + HoTen + "; Tiền điện: " + TienDien + " VNĐ";
});
